module.exports = {
  env: {
    NODE_ENV: '"development"',
    API_HOST: '"https://clx-be-staging.chatlabs.net"',
    // H5_HOST: '"http://10.10.18.87:10086/"'ss
  },
  defineConstants: {
  },
  mini: {},
  h5: {
    devServer: {
      host: 'localhost',
      port: 10086,
      proxy: {
        '/api': { // rest服务的path
          target: 'https://clx-be-staging.chatlabs.net', // 服务端地址
          changeOrigin: true
        }
      }
    },
  }
}
