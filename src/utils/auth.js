import Cache from './cache'
import Util from './util'
import Global from './global'
import Dialog from './dialog'
//缓存 该缓存只在本文件调用
let _storage = {}
Cache.defineProperty(_storage, 'authExpire')
Cache.defineProperty(_storage, 'authSign')
Cache.defineProperty(_storage, 'authMemberId')
Cache.defineProperty(_storage, 'authMemberInfo')
Cache.defineProperty(_storage, 'sessionId')
Cache.defineProperty(_storage, 'userType')
Cache.defineProperty(_storage, 'mineBase')
Cache.defineProperty(_storage, 'inviteCode')
//用户授权
let auth = {}

//加密钥匙
const _key = 'user.dreamstech.cn'

//过期时间，毫秒
const _timeout = 604800000 //7 * 24 * 60 * 60 * 1000

//获取签名 
let _getSign = (expire, memberId) => {
  return Util.md5(memberId + '.' + expire + '.' + _key)
}

//检测签名
auth._checkSign = () => {
  let loginExpire = _storage.authExpire || 0
  let loginSign = _storage.authSign || ''
  let memberId = auth.getMemberId()
  if (loginExpire > 0 && loginSign != '' && memberId > 0) {
    let timestamp = Date.parse(new Date())
    let sign = _getSign(loginExpire, memberId)
    let expire = timestamp - _timeout
    if (loginSign == sign && loginExpire >= expire) {
      //签名一致并在有效时间内
      return true
    }
  }
  return false
}

//获取inviteCode
auth.getInviteCode = ()=>{
	return _storage.inviteCode || null
}

//保存inviteCode
auth.setInviteCode = (data) => {
	if(data){
		_storage.inviteCode = data
	}
}
//删除inviteCode
auth.removeInviteCode = () =>{
	Cache.remove('inviteCode')
}

//保存我的界面数据
auth.getMineBase = (data) => {
  if(data){
	  _storage.mineBase = data
  }
  return _storage.mineBase
}

//检测是否是推广人
auth.isPromoter = (show) => {
	let mineBase = _storage.mineBase || '';
	if(mineBase&&mineBase.distributor&&mineBase.distributor.status=='PASSED'){
		return true;
	}else {
		if(!show){
			// let status = '未申请';
			// if(mineBase&&mineBase.distributor){
			// 	switch(mineBase.distributor.status){
			// 		case 'REVIEW_WAIT': status = '待审核';
			// 		break;
			// 		case 'REJECTED' :status = '已拒绝';
			// 		break;
			// 		case 'DISABLE': status = '已禁用';
			// 		break;
			// 		case 'NO_APPLY': status = '未申请';
			// 		break;
			// 		case 'PASSED': status = '已通过';
			// 		break;
			// 	}
			// }
			let statusTxt = '未申请';
			let reason = null;
			if(mineBase&&mineBase.distributor){
				statusTxt = mineBase.distributor.statusTxt;
				reason = mineBase.distributor.reason
			}
			Dialog.alert({
				content:`推广人员${statusTxt}${reason?"("+reason+")":''}`,
				confirmText: '去申请',
				showCancel: true,
				success: (res) => {
					if (res.confirm) {
						uni.navigateTo({
							url: '/pages/applyToPromotion'
						});
					}
				}
			})
		}
		return false;
	}
}

//是否登录
auth.isLogin = () => {
  return auth._checkSign()
}

/*
 * 获取用户状态(当且仅当用户登录时,即有缓存信息,能获取到 userType>0 )
 * @return {int} userType {-1:两者都不是; 1: 会员; 2:游客}
 */
auth.getUserType = () => {
  if(auth._checkSign()){
    return _storage.userType
  }
  return Global.userType.none
}

//是否登录 调用方式 auth.isAuth
Object.defineProperty(auth,'isAuth',{
  configurable:false,
  get: ()=>{
    return auth.getUserType() == Global.userType.member
  }
})
// 获取用户状态 auth.appUserType
Object.defineProperty(auth,'appUserType',{
  configurable:false,
  get: ()=>{
    return auth.getUserType()
  }
})

//登录 
auth.login = (userType, sessionId, memberId, memberInfo) => {
  let expire = Date.parse(new Date())
  let sign = _getSign(expire, memberId)
  _storage.authExpire = expire
  _storage.authSign = sign
  _storage.authMemberId = memberId
  _storage.authMemberInfo = memberInfo
  _storage.sessionId = sessionId
  _storage.userType = userType
}

//退出
auth.logout = () => {
  Cache.clear()
  // _storage.authExpire = 0
  // _storage.authSign = ''
  // _storage.authMemberId = 0
  // _storage.authMemberInfo = {}
  // _storage.sessionId = ''
  // _storage.userType = ''
}

auth.getSessionId = () => {
  // return "{\"loginId\":\"12052\",\"name\":\"t00001\",\"dtype\":\"MemberLogin\",\"userId\":\"12052\"}"
  return _storage.sessionId || ''
}

//会员id
auth.getMemberId = () => {
  return _storage.authMemberId || 0;
}

//会员信息
auth.getMemberInfo = () => {
  return _storage.authMemberInfo || {}
}

auth.setMemberInfo = (memberInfo) => {
  _storage.authMemberInfo = memberInfo
}

export default auth