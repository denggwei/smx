// 公共方法
import Taro from "@tarojs/taro";
import { useEffect } from 'react'

export function useAsyncEffect (effect, deps) {
  useEffect(() => {
    effect()
  }, deps)
}

// export let isMiniprogramEnv = false;
// if (wx.miniProgram && wx.miniProgram.getEnv) {
//   wx.miniProgram.getEnv(function(res) {
//     // console.log("res.miniprogram", res.miniprogram);
//     isMiniprogramEnv = res.miniprogram;
//   });
// }

export const env = "";
if (process.env.TARO_ENV === "h5") {
  const c = /c=(.{1,}?)(&|#|$)/.test(window.location.href) ? RegExp.$1 : "";
  if (!isMiniprogramEnv && !c) {
    env = /state=(.{1,}?)(&|#|$)/.test(window.location.href)
      ? RegExp.$1
      : "qw2pro";
    env = /-/.test(env) ? env.split("-").find(val => /qw/.test(val)) : env;
  }
}

export const navigateTo = obj => {
  if (!env) return Taro.navigateTo(obj);
  Taro.navigateTo({
    ...obj,
    url: /\?/.test(obj.url)
      ? `${obj.url}&state=${env}`
      : `${obj.url}?state=${env}`
  });
};

export const redirectTo = obj => {
  if (!env) return Taro.redirectTo(obj);
  Taro.redirectTo({
    ...obj,
    url: /\?/.test(obj.url)
      ? `${obj.url}&state=${env}`
      : `${obj.url}?state=${env}`
  });
};

export function funcUrlDel(names) {
  if (typeof names == "string") {
    names = [names];
  }
  let loca = window.location;
  let obj = {};
  let arr = loca.search.substr(1).split("&");
  //获取参数转换为object
  for (let i = 0; i < arr.length; i++) {
    arr[i] = arr[i].split("=");
    obj[arr[i][0]] = arr[i][1];
  }
  //删除指定参数
  for (let i = 0; i < names.length; i++) {
    delete obj[names[i]];
  }
  //重新拼接url
  let url =
    loca.origin +
    loca.pathname +
    "?" +
    JSON.stringify(obj)
      .replace(/[\"\{\}]/g, "")
      .replace(/\:/g, "=")
      .replace(/\,/g, "&");
  return url;
}

// 对比版本号
export function compareVersion(now, old) {
  if (now && old) {
    //将两个版本号拆成数字
    var arr1 = now.split("."),
      arr2 = old.split(".");
    var minLength = Math.min(arr1.length, arr2.length),
      position = 0,
      diff = 0;
    //依次比较版本号每一位大小，当对比得出结果后跳出循环（后文有简单介绍）
    while (
      position < minLength &&
      (diff =
        parseInt(arr1[position].replace(/[a-zA-Z]/, "")) -
        parseInt(arr2[position].replace(/[a-zA-Z]/, ""))) == 0
    ) {
      position++;
    }
    diff = diff != 0 ? diff : arr1.length - arr2.length;
    //若now大于old，则返回true
    return diff > 0;
  } else {
    //输入为空
    console.log("版本号不能为空");
    return false;
  }
}

export function isNewVersion(now, old) {
  if (!now || !old) return false;
  const reg = /(\d+)\.|$/;
  reg.test(now);
  let newNum = +RegExp.$1;
  reg.test(old);
  let oldNum = +RegExp.$1;
  if (newNum > oldNum) return true;
  if (newNum < oldNum) return false;
  return isNewVersion(now.replace(reg, ""), old.replace(reg, ""));
}

/**
 * 节流
 * @param {*} fn 将执行的函数
 * @param {*} time 节流规定的时间
 */
export const throttle = (fn, time) => {
  let timer = null
  return (...args) => {
    // 若timer === false，则执行，并在指定时间后将timer重制
    if(!timer){
      fn.apply(this, args)
      timer = setTimeout(() => {
        timer = null
      }, time)
    }
  }
}


export default {
  isMiniprogramEnv,
  env,
  navigateTo,
  redirectTo,
  funcUrlDel,
  compareVersion,
  isNewVersion,
  throttle
};
