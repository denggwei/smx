
import Global from './global'


//打印日志工具类
let log = {}

log.e = (obj1, obj2) => { 
  if(!Global.server.isDebug) return
  if(obj1 == null && obj2 == null) return;
  let prefix = log.dateFormat(new Date(), 'yyyy-MM-dd HH:mm:ss') + ":  ";
  if(obj2 == null) console.error(prefix, obj1);
  else if(obj1 != null && obj2 != null) console.error(prefix + obj1, obj2);
}

log.i = (obj1, obj2) => { 
  if(!Global.server.isDebug) return
  if(obj1 == null && obj2 == null) return;
  let prefix = log.dateFormat(new Date(), 'yyyy-MM-dd HH:mm:ss') + ":  ";
  if(obj2 == null) console.log(prefix, obj1);
  else if(obj1 != null && obj2 != null) console.log(prefix + obj1, obj2);
}

/** 对外，格式化时间 时间戳转化为年 月 日 时 分 秒
   * @param  {Date} time   传入时间(Date类型)
   * @param  {String} format 返回格式，支持自定义，如yyyyMMdd
   * @return {String}        返回转化后的数据
   */
log.dateFormat = (time, format) => {
	var formateArr = ['yyyy', 'MM', 'dd', 'HH', 'mm', 'ss'];
	var returnArr = [];
	var date = time == null ? new Date() : time;
	returnArr.push(date.getFullYear());
	returnArr.push(log.formatNumber(date.getMonth() + 1));
	returnArr.push(log.formatNumber(date.getDate()));
	returnArr.push(log.formatNumber(date.getHours()));
	returnArr.push(log.formatNumber(date.getMinutes()));
	returnArr.push(log.formatNumber(date.getSeconds()));
	for (var i in returnArr) {
	  format = format.replace(formateArr[i], returnArr[i]);
	}
	return format;
}
/** 把个位数转化成 0x模式
* @param  {int} n 数字
* @return {String}   转化后的数据
*/
log.formatNumber = (n) => {
	n = n.toString()
	return n[1] ? n : '0' + n
}

export default log
