let global = {
	phone:'',
	loginByCode:'',
	server: {}, // 服务配置
	defShareTitle:'萧邦线上精品店',

	// 平台
	platform: {
		h5: 'h5',
		alipay: 'mp-alipay', //支付宝小程序
		baidu: 'mp-baidu', //百度小程序
		weixin: 'mp-weixin', //微信小程序
		toutiao: 'mp-toutiao', //头条小程序
		qq: 'mp-qq', //qq 小程序
	},

	// 身份编号
	userType: {
		none: -1, // 两者都不是
		member: 1, // 会员
		tourist: 2 // 游客
	},

	// 下单标识
	orderFlag: {
		normal: 1, // 立即购买，正常流程
		card: 2, // 购物车
	},

	//支付标识
	payFlag: {
		normal: 1, //正常购买支付
		order: 2 //订单详情/订单列表支付
	},

	orderState: {
		all: 'ALL',
		waitForPay: 'PENDING_PAYMENT', // 待付款
		waitForReview: 'PENDING_REVIEW', // 等待审核
		denied: 'DENIED', // 已拒绝
		waitForDelive: 'PENDING_SHIPMENT', // 等待发货
		waitForReceive: 'SHIPPED', // 运输中, 待收货
		waitForAppraise: 'RECEIVED', // 已收货, 待评价
		failed: 'FAILED', // 已失败, 可删除
		cancel: 'CANCELED', // 已取消, 已关闭
		expired: 'EXPIRED', // 已过期
		finished: 'COMPLETED', // 已完成
		waitRefundReview: 'PENDING_RETURN_REVIEW', // 等待退货审核
		colse: 'CLOSE', // 已关闭

		saled_pending: 'SALED_PENDING', // 等待审核
		saled_approved: 'SALED_APPROVED', // 审核通过
		saled_failed: 'SALED_FAILED', // 审核失败
		saled_completed: 'SALED_COMPLETED', // 已完成
		saled_canceled: 'SALED_CANCELED', // 已取消
		waiteForTake: 'PENDING_SHIPMENT', // 待取货
	},
	orderStateName: {
		'ALL': '全部订单',
		'PENDING_PAYMENT': '待付款',
		'DENIED': '已拒绝',
		'PENDING_REVIEW': '等待审核',
		'PENDING_SHIPMENT': '等待发出',
		'SHIPPED': '待收件',
		'RECEIVED': '已收件',
		'FAILED': '已失败',
		'CANCELED': '已取消',
		'EXPIRED': '已过期',
		'COMPLETED': '已完成',
		'PENDING_RETURN_REVIEW': '等待售后审核',
		'CLOSE': '已关闭',
		'PENDING_SHIPMENT': '待取件'
	},

	// 售后状态
	saledStatus: { // 接口返回的状态 映射到 本应用的状态
		// PENDING: 'SALED_PENDING', // 等待审核
		// APPROVED: 'SALED_APPROVED', // 审核通过
		// FAILED: 'SALED_FAILED', // 审核失败
		// COMPLETED: 'SALED_COMPLETED', // 已完成
		// CANCELED: 'SALED_CANCELED', // 已取消
		PENDING                  :'PENDING',//等待审核
		APPROVED                 :'APPROVED',//审核通过
		FAILED                   :'FAILED',//审核失败
		COMPLETED                :'COMPLETED',//已完成
		CANCELED                 :'CANCELED',//已取消
		PAYOK                    :'PAYOK',//待确认支付
		WaitPickUp               :'WaitPickUp',//待取件
		PickUpIng                :'PickUpIng',//取件中
		Received                 :'Received',//已收件
		InspectionIng            :'InspectionIng',//验货中
		InspectionOk             :'InspectionOk',//验货通过
		InspectionFail           :'InspectionFail',//验货失败
		Refunded                 :'Refunded',//已退款
		WaitExchangeGoods        :'WaitExchangeGoods',//待换货
		WaitReturn               :'WaitReturn',//待返还
		ExchangeGoodsShipped     :'ExchangeGoodsShipped',//换货发货
		ReturnShipped            :'ReturnShipped',//返还发货
		ExchangeGoodsShippedIng  :'ExchangeGoodsShippedIng',//换货已发货
		ReturnShippedIng         :'ReturnShippedIng',//返还已发货
		ExchangeGoods            :'ExchangeGoods',//已换货
		Returned                 :'Returned',//已返还
		OtherScheme              :'OtherScheme',//其它方案
	},
	saledStatusName: {
		PENDING: '等待审核',
		APPROVED: '审核通过',
		FAILED: '审核失败',
		COMPLETED: '已完成',
		CANCELED: '售后已取消',
	},
	// 售后类型
	saledType: {
		AFTERSALES_REPAIR: 'AFTERSALES_REPAIR',
		AFTERSALES_REPLACEMENT: 'AFTERSALES_REPLACEMENT',
		AFTERSALES_RETURNS: 'AFTERSALES_RETURNS'
	},
	saledTypeName: {
		"AFTERSALES_REPAIR": '维修',
		"AFTERSALES_REPLACEMENT": '换货',
		"AFTERSALES_RETURNS": '退货'
	},
	
	//小程序业务域名
	businessDomain: [
		'https://apis.map.qq.com',
		'https://chopard-ec.chatlabs.net',
		'https://thirdwx.qlogo.cn',
		'https://wx.gtimg.com'
	]
}
export default global
