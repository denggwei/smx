import { Component, useState } from 'react'
import Taro from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import Cheshi from '@/components/cheshi'
import './index.scss'

export default function Index() {
  const [loading, setLoading] = useState(true)
  const [threads, setThreads] = useState([])
  return (
    <View className='index'>
      <Text>Hello world!</Text>
      <View onClick={() => Taro.navigateTo({
        url: 'pages/gategory/index'
      })}>跳转</View>
      <Cheshi></Cheshi>
    </View>
  )
}
