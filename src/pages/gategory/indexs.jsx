import { Component, useEffect, useMemo, useState, useRef } from 'react'
import { useRequest, useRouter, } from 'taro-hooks';
import { useReady, usePullDownRefresh, usePageScroll } from '@tarojs/taro';
import { View, Text, Block, ScrollView } from '@tarojs/components'
import Cheshi from '@/components/cheshi'
import Header from '@/components/Header/index.jsx'
import ShopList from '@/components/ShopList/index.jsx'
import BackTop from '../../components/BackTop';
import BtnGroup from '@/components/BtnGroup/index.jsx'
import Filter from '../../components/Filter';
import { AtAccordion, AtList } from 'taro-ui'
import Collapsible from 'react-collapsible';

import './index.scss'

// test IMAGE
import banner from '@/assets/images/aw0lm-fizy9.png'
import classify from '@/assets/images/classify.png'
import search from '@/assets/images/search.png'
import user from '@/assets/images/user.png'
import buyCar from '@/assets/images/buyCar.png'
import logo from '@/assets/images/logo.png'



export default function Index() {
  const router = useRouter()

  const scrollEle = useRef(null)
  const filterBtn = [{ name: '全部', show: false }, { name: '新品', show: false }, { name: '价格', show: false }, { name: '筛选', show: true }]
  const trueBtn = [{
    name: '确认',
    style: {
      width: 'calc(320vmin * 100 / 750)',
    }
  }]
  const resetBtn = [{
    name: '重置',
    style: {
      width: 'calc(320vmin * 100 / 750)',
    }
  }]
  const [pageHight, setPageHight] = useState(null);
  const [pageData, setPageData] = useState(null);
  const [initPageData, setInitPageData] = useState(null);
  const [filterList, setFilterList] = useState(null);
  const [initFilterList, setInitFilterList] = useState(null);
  const [scrollTop, setScrollTop] = useState(0)
  const [total, setTotal] = useState(0)
  const [sortMap, setSortMap] = useState([
    {
      label: '最新',
      value: 'newest',
    },
    {
      label: '价格从低到高',
      value: 'price_asc',
    },
    {
      label: '价格从高到低',
      value: 'price_desc',
    },
  ]);
  const [formData, setFormData] = useState({
    page: '1',
    size: '10',
    sort: sortMap[0].value,
  })
  const [shopData, setShopData] = useState(null)
  const [filterlight, setFilterlight] = useState('全部')
  const [filterShow, setFilterShow] = useState(false)

  const { run: getProduct } = useRequest(
    {
      url: `/api/product`,
      method: 'get',
      data: formData
    },
    // {
    //   manual: true
    // },
    {
      manual: true,
      onSuccess(res) {
        let data = res.data.data;
        // 所有数据
        setPageData(data);

        // 储存初始数据
        if(!initPageData){
          setInitPageData(data)
        }

        // 总数据条数
        setTotal(data.total_count)

          // 商品数据
        shopData ? setShopData([...shopData, ...res.data.data.product_list]) : setShopData([...res.data.data.product_list])
      },
    },
  )

  const onClickFilter = ((target, item) => {
    let _filterData = JSON.parse(JSON.stringify(filterList))
    let __filterData = _filterData.map((i, idx) => {
      if (i.name === item.name) {
        i.child[target].light = !i.child[target].light
      }
      return i
    })
    switch (item.type) {
      case 'brand':
        setFormData({
          ...formData,
          brand_id_list: item.child[target].id
          // formData.brand_id_list ? 
          //   (() => { 
          //     console.log('formData.brand_id_list', formData.brand_id_list);
          //     const bol = formData.brand_id_list.some((i) => { return i == item.child[target].id }) 
          //   return bol ? JSON.stringify([...formData.brand_id_list]) : JSON.stringify([...formData.brand_id_list, item.child[target].id])})() : JSON.stringify([item.child[target].id]),
        });
        break;
      case 'series':
        setFormData({
          ...formData,
          series_id_list: item.child[target].id
          // formData.series_id_list ? [...formData.series_id_list, item.child[target].id] : [item.child[target].id],
        });
        break;
      case 'attribute':
        setFormData({
          ...formData,
          attribute_value_list: item.child[target].name
          // formData.attribute_value_list ? [...formData.attribute_value_list, item.child[target].name] : [item.child[target].name],
        });
        break;
      case 'specification':
        setFormData({
          ...formData,
          specification_value_list: item.child[target].name
          // formData.specification_value_list ? [...formData.specification_value_list, item.child[target].name] : [item.child[target].name],
        });
        break;
    }
    setFilterList([...__filterData])
    // console.log('__filterData', __filterData);
  })

  const FilterDom = ((item) => {
    return (
      <View className='FilterList'>
        {
          item && item.child.map((i, idx) => {
            return (
              <View className='item' key={idx}>
                <View
                  className='FilterDom'
                  style={i.option ? i.light ? { background: 'black', color: 'white' } : {} : { color: '#f3f3f3' }}
                  onClick={(e) => onClickFilter(idx, item)}
                >{i.name}</View>
              </View>
            )
          })
        }
      </View>
    )
  })

  const accordComp = useMemo(() => {
    const data = filterList && [...filterList]
    console.log('filterList', filterList, initFilterList);
    return (
      data && data.length > 0 && data.map((item, idx) => {
        return (
          (<AtAccordion
            key={idx}
            title={item.name}
            hasBorder={false}
            open={item.open}
            onClick={() => onAccordion(item.name)}
          >{
              FilterDom(item)
            }</AtAccordion>)

        )
      })
    )
  }, [setFilterList, onClickFilter]);

  const filterDataFn = ((data) => {
    console.log('data', data, filterList);
    let _pageData = data && data && Object.keys(data).length > 0 && Object.keys(data).filter((i, idx) => {
      return i === 'brand_list' || i === 'series_list' || i === 'attribute_list' || i === 'specification_list'
    })
    let _filterData = []
    _pageData && _pageData.map((i, idx) => {
      let obj = {
        open: true,
      }
      switch (i) {
        case 'brand_list':
          obj.name = '品牌'
          obj.child = initPageData[i] && initPageData[i].map((item, idx) => {
            const option = data[i] && data[i].some((val)=>{
              return val.name === item.name
            })
            let a = { ...item, light: false, option: option }
            return a
          })
          obj.type = 'brand'
          obj.child && _filterData.push(obj)
          break;
        case 'series_list':
          obj.name = '商品系列'
          obj.child = initPageData[i] && initPageData[i].map((item, idx) => {
            const option = data[i] && data[i].some((val)=>{
              return val.name === item.name
            })
            let a = { ...item, light: false, option: option }
            return a
          })
          obj.type = 'series'
          obj.child && _filterData.push(obj)
          break;
        case 'attribute_list':
          initPageData[i] && Object.keys(initPageData[i]).length > 0 && Object.keys(initPageData[i]).forEach((item) => {
            let _obj = { ...obj }
            _obj.name = item
            _obj.child =  initPageData[i] && initPageData[i][item].map((x, idx) => {
              const option = data[i][item] && data[i][item].some((val)=>{
                return val.name === x.name
              })
              console.log('option', option)
              let a = { name: x, light: false, option: option }
              return a
            })
            _obj.type = 'attribute'
            _filterData.push(_obj)
          })
          break;
        case 'specification_list':
          initPageData[i] && Object.keys(initPageData[i]).length > 0 && Object.keys(initPageData[i]).forEach((item) => {
            let _obj = { ...obj }
            _obj.name = item
            _obj.child =  initPageData[i] && initPageData[i][item].map((x, idx) => {
              const option = data[i][item] && data[i][item].some((val)=>{
                return val.name === x.name
              })
              let a = { name: x, light: false, option: option }
              return a
            })
            _obj.type = 'specification'
            _filterData.push(_obj)
          })
          break;
      }
    })
    console.log('_filterData', _filterData);
    if(!initFilterList){
      setInitFilterList(_filterData)
    }
    setFilterList(_filterData)
  })

  const onAccordion = ((name) => {
    
  })

  const getProductFn = async () => {
    // console.log('router', router[0].params);
    let res = await getProduct()
    // if(router[0].params.id){
    //   if(router[0].params.type === '系列'){
    //     setFormData({...formData, series_id_list: [router[0].params.id]})
    //     console.log('formData', formData)
    //     res = await getProduct()
    //   }else if(router[0].params.type === '分类'){
    //     setFormData({...formData, series_id_list: [router[0].params.id]})
    //     res = await getProduct()
    //   }
    // }
    // if (res.data.data) {
    //   let data = JSON.parse(JSON.stringify(res.data.data))
    //   delete data.product_list
      // const shopData = Taro.getStorageSync('shopData')
      // if(shopData){
      //   filterDataFn(shopData)
      // }else{
      //   filterDataFn(data)
      //   Taro.setStorageSync('shopData', data)
      // }
      // 
      // setTotal(data.total_count)
    
    // }
  }

  const onShowFilter = ((item, type) => {
    if (item.name === '价格') {
      if (formData.sort === 'price_desc') {
        setFormData({ ...formData,    page: '1',
        size: '10', sort: 'price_asc' })
      } else {
        setFormData({ ...formData,    page: '1',
        size: '10', sort: 'price_desc' })
      }
    } else if (item.name === '新品' || item.name === '全部') {
      setFormData({ sort: 'newest',     page: '1',
      size: '10', })
    } else {
      setFormData({ ...formData,    page: '1',
        size: '10'})
    }
    setFilterlight(item.name)
    setFilterShow(item.show)
  })

  const onFormData = ((val, item) => {
    console.log('调用', val, item)
  })

  // const FilterDom = useMemo(() => {
  //   return (
  //     <Filter
  //       // className
  //       pageData={pageData}
  //       show={filterShow}
  //       onChange={onFormData}
  //     ></Filter>
  //   )
  // }, [{ ...pageData }, onShowFilter])

  const FilterBtn = useMemo(() => {
    return <View className='filterBtn'>
      {
        filterBtn.map((item, idx) => {
          return <View
            key={idx}
            className='item'
            style={{ color: item.name == filterlight ? '#000000' : '#8E8E8E' }}
            onClick={() => onShowFilter(item)}
          >{item.name}</View>
        })
      }
    </View>
  }, [onShowFilter,])

  const onScrollToLower = ((e) => {
    console.log('scrollTop', scrollTop)
    const pageTotal = Math.ceil(total / 10);
    let newPageNum = Number(formData.page) + 1
    if (newPageNum > pageTotal) {
      // console.log('newPageNum', newPageNum, pageTotal)
      // Taro.showToast({
      //   title: '无更多数据'
      // })
    } else {
      setFormData({ ...formData, page: newPageNum })
      // Taro.hideLoading()
    }

  })

  useEffect(() => {
    if (router[0].params.id) {
      // if (router[0].params.type === '系列') {
      //   setFormData({ ...formData, series_id_list: [router[0].params.id] })
      // } else if (router[0].params.type === '分类') {
      setFormData({ ...formData, category_id: router[0].params.id })
      // }
    }
    console.log('scrollEle', scrollEle.current.offsetHeight);
    if (scrollEle) {
      setPageHight(scrollEle.current.offsetHeight)
    }
    // if (box) {
    //   setPageHight(box.offsetHeight)
    // }
  }, [])

  useEffect(() => {
    console.log('formData', formData);
    getProductFn()
  }, [formData])

  useEffect(() => {
    if (initPageData) {
      // if (shopData) {
      //   filterDataFn(shopData.data)
      // } else {
      //   filterDataFn(pageData.data)
      // }
      console.log('pageData', pageData);
      filterDataFn(pageData)
    }
  }, [])

  useEffect(() => {
    setScrollTop(() => {
      return 0 + Math.random()
    })
  }, [setPageHight])


  return (
    <View className='Gategory'>
      <Header
        leftUrl={[{ url: classify, type: 'Search' }, { url: search, type: '' }]}
        logo={logo}
        rightUrl={[{ url: user, type: '' }, { url: buyCar, type: '' }]}
      ></Header>
      <ScrollView
        className='scrollview'
        ref={scrollEle}
        style={pageHight ? { height: pageHight } : {}}
        scrollY
        scrollTop={scrollTop}
        scrollWithAnimation
        onScrollToLower={() => onScrollToLower()}
      >
        {
          !filterShow && <img
            style={{ width: '100%' }}
            src={banner}
          ></img>
        }
        {
          FilterBtn
        }
        {
          filterShow && <View className='Filter'>
            <View className='content'>
              {
                accordComp
              }
              {/* <BtnGroup
                className="btnGroup"
                styleBtn={{ padding: 'calc(48vmin * 100 / 750) calc(40vmin * 100 / 750)' }}
                btn1={trueBtn[0]}
                btn2={resetBtn[0]}
              ></BtnGroup> */}
            </View>
          </View>
        }
        {shopData && <ShopList
          List={shopData}
        ></ShopList>}

        <BackTop
          onBack={()=>setScrollTop(() => {
            return 0 + Math.random()
          })}
        ></BackTop>
      </ScrollView>
    </View>
  )
}