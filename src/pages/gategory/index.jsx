import { Component, useEffect, useMemo, useState, useRef, useCallback } from 'react'
import { useRequest, useRouter, } from 'taro-hooks';
import { useReady, usePullDownRefresh, usePageScroll } from '@tarojs/taro';
import { View, Text, Block, ScrollView, Switch } from '@tarojs/components'
import Cheshi from '@/components/cheshi'
import Header from '@/components/Header/index.jsx'
import ShopList from '@/components/ShopList/index.jsx'
import BackTop from '../../components/BackTop';
import BtnGroup from '@/components/BtnGroup/index.jsx'
import Filter from '../../components/Filter';
import { AtAccordion, AtList } from 'taro-ui'
import Collapsible from 'react-collapsible';

import './index.scss'

// test IMAGE
import banner from '@/assets/images/aw0lm-fizy9.png'
import classify from '@/assets/images/classify.png'
import search from '@/assets/images/search.png'
import user from '@/assets/images/user.png'
import buyCar from '@/assets/images/buyCar.png'
import logo from '@/assets/images/logo.png'

export default function Index() {
  const router = useRouter()
  const scrollEle = useRef(null)

  const [filterBtn, setFilterBtn] = useState([{ name: '全部', show: true, }, { name: '新品', show: false, }, { name: '价格', show: false, }, { name: '筛选', show: false }])
  // 滚动
  const [pageHight, setPageHight] = useState(null);
  const [scrollTop, setScrollTop] = useState(0)
  const [total, setTotal] = useState(0)

  const [pageData, setPageData] = useState(null);
  const [everPageData, setEverPageData] = useState(null);
  const [filterList, setFilterList] = useState({});

  const [params, setParams] = useState({
    page: '1',
    size: '100',
    category_id: router[0].params.id,
    sort: 'newest',
  });
  const [filterHidden, setFilterHidden] = useState(false);
  const [filter, setFilter] = useState([]);
  const [checked, setChecked] = useState(false);
  const { data, error, loading, run } = useRequest(
    {
      url: 'api/product',
      method: 'post',
      data: params,
    },
    {
      manual: true,
      onSuccess(res) {
        let data = res.data.data;
        setPageData(data);

        let obj = {};
        if (!!data.brand_list && data.brand_list.length) {
          let arr = [];
          data.brand_list.map((item) => {
            arr.push(item.name);
          });
          obj['brand_list'] = arr;
        }
        if (!!data.series_list && data.series_list.length) {
          let arr = [];
          data.series_list.map((item) => {
            arr.push(item.name);
          });
          obj['series_list'] = arr;
        }
        if (!!data.attribute_list) {
          let arr = [];
          Object.keys(data.attribute_list).map((item) => {
            data.attribute_list[item].map((child, childIndex) => {
              arr.push(child);
            });
          });
          obj['attribute_list'] = arr;
        }
        if (!!data.specification_list) {
          let arr = [];
          Object.keys(data.specification_list).map((item) => {
            data.specification_list[item].map((child, childIndex) => {
              arr.push(child);
            });
          });
          obj['specification_list'] = arr;
        }
        setFilterList(obj);
        // 总数据条数
        setTotal(data.total_count)
        console.log('data', data, everPageData);
        if (!everPageData) {
          setEverPageData(data);
        }

      },
    },
  );

  const onShowFilter = ((item, type) => {
    let _filterBtn = []
    switch (item.name) {
      case '全部':
        _filterBtn = filterBtn.map((item, idx) => {
          if (item.name === '全部') {
            item.show = true
          } else if (item.name === '新品' || item.name === '价格') {
            item.show = false
          }
          return item
        })
        setFilterBtn(_filterBtn)
        setParams({
          sort: 'newest', page: '1',
          size: '10',
        })
        setFilterHidden(false)
        break;
      case '新品':
        _filterBtn = filterBtn.map((item, idx) => {
          if (item.name === '新品') {
            item.show = true
          } else if (item.name === '全部' || item.name === '价格') {
            item.show = false
          }
          return item
        })
        setFilterBtn(_filterBtn)
        setParams({
          sort: 'newest', page: '1',
          size: '10',
        })
        setFilterHidden(false)
        break;
      case '价格':
        _filterBtn = filterBtn.map((item, idx) => {
          if (item.name === '价格') {
            item.show = true
          } else if (item.name === '全部' || item.name === '新品') {
            item.show = false
          }
          return item
        })
        setFilterBtn(_filterBtn)
        if (params.sort === 'price_desc') {
          setParams({
            ...params, page: '1',
            size: '10', sort: 'price_asc'
          })
        } else {
          setParams({
            ...params, page: '1',
            size: '10', sort: 'price_desc'
          })
        }
        setFilterHidden(false)
        break;
      case '筛选':
        _filterBtn = filterBtn.map((item, idx) => {
          if (item.name === '筛选') {
            item.show = true
          }
          return item
        })
        setFilterBtn(_filterBtn)
        setParams({
          ...params, page: '1',
          size: '10'
        })
        setFilterHidden(true)
        break;
    }
    // setFilterlight(item.name)
    // setFilterShow(item.show)
  })

  const onScrollToLower = ((e) => {
    console.log('scrollTop', scrollTop)
    const pageTotal = Math.ceil(total / 10);
    let newPageNum = Number(params.page) + 1
    if (newPageNum > pageTotal) {
      // console.log('newPageNum', newPageNum, pageTotal)
      // Taro.showToast({
      //   title: '无更多数据'
      // })
    } else {
      setParams({ ...params, page: newPageNum })
      // Taro.hideLoading()
    }

  })
  const FilterDom = useCallback((item) => {
    console.log('item', item);
    return (
      <View className='FilterList'>
        {
          item?.map((i, idx) => {
            return (
              <View className='item' key={idx}>
                <View
                  className='FilterDom'
                  // style={i.option ? i.light ? { background: 'black', color: 'white' } : {} : { color: '#f3f3f3' }}
                  // onClick={(e) => onClickFilter(idx, item)}
                >{i.name || i}</View>
              </View>
            )
          })
        }
      </View>
    )
  })

  const accordComp = useCallback(
    (arr, title, type) => {
      // console.log('arr, title, type', arr,type, title, filterList);
      let currentFilterList = [];
      let idsList = [];
      switch (type) {
        case 'brand':
          currentFilterList = filterList?.brand_list || [];
          idsList = params?.brand_id_list || [];
          break;
        case 'series':
          currentFilterList = filterList?.series_list || [];
          idsList = params?.series_id_list || [];

          break;
        case 'attribute':
          currentFilterList = filterList?.attribute_list || [];
          idsList = params?.attribute_value_list || [];

          break;
        case 'specification':
          currentFilterList = filterList?.specification_list || [];
          idsList = params?.specification_value_list || [];

          break;
      }
          return (
            (<AtAccordion
              key={title}
              title={title}
              hasBorder={false}
              open={true}
              // open={item.open}
              // onClick={() => onAccordion(item.name)}
            >{
                FilterDom(arr)
              }</AtAccordion>)

          )
    },
    [filterList, params],
  );

  const FilterView = useMemo(() =>{
    return(
      <View className='Filter'>
        <View
          className='content'
        >
          {accordComp(everPageData?.brand_list, '品牌', 'brand')}
      {accordComp(everPageData?.series_list, '商品系列', 'series')}
      {Object.keys(everPageData?.attribute_list || {}).map((item) => {
        return accordComp(
          everPageData?.attribute_list[item],
          item,
          'attribute',
        );
      })}
      {Object.keys(everPageData?.specification_list || {}).map(
        (item) => {
          return accordComp(
            everPageData?.specification_list[item],
            item,
            'specification',
          );
        },
      )}


        </View>
    </View>
    )
  }, [everPageData])

  const FilterBtn = useMemo(() => {
    return <View className='filterBtn'>
      {
        filterBtn.map((item, idx) => {
          return <View
            key={idx}
            className='item'
            style={{ color: item.show ? '#000000' : '#8E8E8E' }}
            onClick={() => onShowFilter(item)}
          >{item.name}</View>
        })
      }
    </View>
  }, [onShowFilter])

  // const LightTooltip = styled(({ className, ...props }) => (
  //   <Tooltip {...props} classes={{ popper: className }} />
  // ))(({ theme }) => ({
  //   [`& .${tooltipClasses.tooltip}`]: {
  //     backgroundColor: theme.palette.common.white,
  //     boxShadow: theme.shadows[1],
  //     color: 'rgba(0, 0, 0, 1)',
  //     padding: theme.spacing(2),
  //   },
  //   [`& .${tooltipClasses.arrow}`]: {
  //     '::before': {
  //       background: theme.palette.common.white,
  //       boxShadow: theme.shadows[1],
  //     },
  //   },
  // }));
  useEffect(() => {
    if (scrollEle) {
      setPageHight(scrollEle.current.offsetHeight)
    }
  }, [])

  useEffect(() => {
    setScrollTop(() => {
      return 0 + Math.random()
    })
  }, [setPageHight])

  useEffect(() => {
    setParams({ ...params, category_id: router[0].params.id });
  }, [router[0].params.id]);

  useEffect(() => {
    run();
  }, [params]);

  return (
    <View className='Gategory'>
      <Header
        leftUrl={[{ url: classify, type: 'Search' }, { url: search, type: '' }]}
        logo={logo}
        rightUrl={[{ url: user, type: '' }, { url: buyCar, type: '' }]}
      ></Header>
      <ScrollView
        className='scrollview'
        ref={scrollEle}
        style={pageHight ? { height: pageHight } : {}}
        scrollY
        scrollTop={scrollTop}
        scrollWithAnimation
        onScrollToLower={() => onScrollToLower()}
      >
        {
          // !filterShow && 
          <img
            style={{ width: '100%' }}
            src={banner}
          ></img>
        }
        {
          FilterBtn
        }
        {filterHidden && (
          FilterView
        )}
        {/* {shopData && <ShopList
          List={shopData}
        ></ShopList>} */}

        <BackTop
          onBack={() => setScrollTop(() => {
            return 0 + Math.random()
          })}
        ></BackTop>
      </ScrollView>
    </View>
  );
}