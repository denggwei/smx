import { Component, useMemo, useState, useEffect} from 'react'
import { useRequest,useRouter } from 'taro-hooks';
import { View, Text, Swiper, SwiperItem, Picker } from '@tarojs/components'
import { AtRate, AtAccordion, AtList, AtListItem } from 'taro-ui'
import Cheshi from '@/components/cheshi'
import Header from '@/components/Header/index.jsx'
import ShopList from '@/components/ShopList/index.jsx'
import BackTop from '../../components/BackTop';
import DropDown from '@/components/DropDown/index.jsx';
import BtnGroup from '@/components/BtnGroup/index.jsx'
import Modal from '@/components/Modal/index.jsx'

import './index.scss'

// test IMAGE
import location from '@/assets/images/形状(1).png'
import classify from '@/assets/images/classify.png'
import search from '@/assets/images/search.png'
import user from '@/assets/images/user.png'
import buyCar from '@/assets/images/buyCar.png'
import logo from '@/assets/images/logo.png'
import wish from '@/assets/images/bag备份 6.png'

import i_free_barter from '@/assets/images/i_free_barter.png'
import i_free_deliver from '@/assets/images/i_free_deliver.png'
import i_pay from '@/assets/images/i_pay.png'
import i_gift from '@/assets/images/i_gift.png'

export default function Index() {
  const router = useRouter()
  const selector = ['美国', '中国', '巴西', '日本']
  const shopCar = [{
    name: '加入购物车',
    style: {
      width: 'calc(320vmin * 100 / 750)',
    }
  },{
    name: '加入购物车',
    style: {
      width: 'calc(200vmin * 100 / 750)',
    }
  }]
  const goBuy = [{
    name: '立即购买',
    style: {
      width: 'calc(320vmin * 100 / 750)',
    }
  },{
    name: '立即购买',
    style: {
      width: 'calc(200vmin * 100 / 750)',
    }
  },]

  const [data, setData] = useState({})
  const [current, setCurrent] = useState(0)
  const [rateNum, setRateNum] = useState(5)
  const [size, setSize] = useState('美国')
  const [openList, setOpen] = useState([{
    title: '商品描述',
    open: false,
    empty: '暂无描述'
  }, {
    title: '商品评论',
    open: false,
    empty: '暂无评论'
  }, {
    title: '商品参数',
    open: false,
    empty: '暂无参数'
  }, {
    title: '商品推荐',
    open: false,
    empty: '暂无推荐'
  }])
  const [keySize, setKeySize] = useState(false)
  const [showSize, setShowSize] = useState(false)
  const [rangeKey, setrangeKey] = useState(0)

  // const data = {
  //   "attributes": [
  //     {
  //       "attribute_id": "",
  //       "attribute_name": "",
  //       "attribute_values": []
  //     }
  //   ],
  //   "catalog_id": "",
  //   "category_ids": [],
  //   "created_at": "",
  //   "currency": "",
  //   "external_spu_code": "",
  //   "max_sell_price": 1000,
  //   "min_sell_price": 1000,
  //   "sku_type": 0,
  //   "skus": [
  //     {
  //       "catalog_id": "",
  //       "cost_price": null,
  //       "created_at": "",
  //       "currency": "",
  //       "external_sku_code": "",
  //       "market_price": null,
  //       "sell_price": null,
  //       "sku_id": "",
  //       "sku_image": "",
  //       "updated_at": "",
  //       "variation_attributes": [
  //         {
  //           "name": "",
  //           "value": ""
  //         }
  //       ],
  //       "volume": null,
  //       "volume_unit": "",
  //       "weight": null,
  //       "weight_unit": ""
  //     }
  //   ],
  //   "spu_description": "",
  //   "spu_detail_html": [],
  //   "spu_id": "",
  //   "spu_images": ["https://clc-1253185145.file.myqcloud.com/swatch/643422dd-567e-471c-a099-28a4d0723a5c_large.jpg", "https://clc-1253185145.file.myqcloud.com/swatch/643422dd-567e-471c-a099-28a4d0723a5c_large.jpg", "https://clc-1253185145.file.myqcloud.com/swatch/643422dd-567e-471c-a099-28a4d0723a5c_large.jpg", "https://clc-1253185145.file.myqcloud.com/swatch/643422dd-567e-471c-a099-28a4d0723a5c_large.jpg"],
  //   "spu_main_image": "",
  //   "spu_name": "Board the mission",
  //   "spu_status": 0,
  //   "spu_status_modified_at": "",
  //   "total_sku": 0,
  //   "updated_at": "",
  //   "variation_attributes": [
  //     {
  //       "variation_attribute_id": "",
  //       "variation_attribute_name": "",
  //       "variation_attribute_values": []
  //     }
  //   ]
  // }

  const comtData = [
    {
      "score": 2,
      "createdDate": "2021-11-30 15:43",
      "imageUrls": ["https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/1a1e9d53-a77c-4b71-92c5-a39103bf8eee.jpg",
        "https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/072fcf0e-3477-45d0-b8ba-94f816da63fe.jpg",
        "https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/5d3a3c30-4fcc-48c3-86ed-9964261d956a.jpg"],
      "member": {
        "icon": "https://thirdwx.qlogo.cn/mmopen/vi_32/6RNZibCZALRGSc8600kb6sY5tWXCmkVsF5Zu7OQzJgZpn3m70icYR0wtCJ9dLHgeiaKpl4YSc3EjwrxtySuIFWysA/132",
        "name": "张三",
        "id": 23752,
        "username": "133****1808"
      },
      "id": 12552,
      "specifications": [
        "默认"
      ],
      "content": "地区的",
      "productName": "燃爆能量"
    },
    {
      "score": 5,
      "createdDate": "2021-11-24 15:58",
      "imageUrls": [
        "https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/1a1e9d53-a77c-4b71-92c5-a39103bf8eee.jpg",
        "https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/072fcf0e-3477-45d0-b8ba-94f816da63fe.jpg",
        "https://swatch-staging-be.chatlabs.net/visit/upload/image/202111/5d3a3c30-4fcc-48c3-86ed-9964261d956a.jpg"
      ],
      "member": {
        "icon": "https://thirdwx.qlogo.cn/mmopen/vi_32/6RNZibCZALRGSc8600kb6sY5tWXCmkVsF5Zu7OQzJgZpn3m70icYR0wtCJ9dLHgeiaKpl4YSc3EjwrxtySuIFWysA/132",
        "name": "张三",
        "id": 23752,
        "username": "133****1808"
      },
      "id": 12403,
      "specifications": [
        "默认"
      ],
      "content": "qfqfqfqefffffffffffffffffffffffffff",
      "productName": "燃爆能量"
    }
  ]

  const ServiceData = [{
    name: '瑞士制造',
    img: i_free_barter,
  }, {
    name: '安心保修',
    img: i_free_deliver,
  }, {
    name: '免费配送',
    img: i_pay,
  }, {
    name: '轻松退货',
    img: i_gift,
  },]

  const { run: getShopDetails } = useRequest(
    {
      url: `/api/product/${router[0].params.spuId}`,
      method: 'get',
    },
    {
      manual: true
    }
  )
  const getShopDetailsFn = async () => {
    let res = await getShopDetails()
    if(res.data) {
      console.log('res.data', res.data);
      setData(res.data.data)
    }
  }

  const onChangeSwiper = ((value) => {
    console.log('value', value);
    setCurrent(value)
  })

  const onSwiper = ((e) => {
    console.log('e.detail.current', e.detail.current);
    setCurrent(e.detail.current)
  })

  const onChangeSize = (e) => {
    // console.log('e', e);
    setSize(selector[e.detail.value])
    setrangeKey(e.detail.value)
  }

  const dropDownClick = (idx) => {
    let _openList = JSON.parse(JSON.stringify(openList))
    _openList[idx].open = !openList[idx].open
    setOpen([..._openList])
  }

  const onShowModal = (value) => {
    setShowSize(value)
  }

  const onShopCar = () => {
    !keySize && onShowModal(true)
  }

  const onBuy = () => {

  }

  const SwiperImg = useMemo(() => {
    return (
      <View className='swiperImg'>
        <View className='shop_head'>
          <img className='new'></img>
          <img className='wish' src={wish}></img>
        </View>
        <Swiper
          className='swiper'
          indicatorColor='#999'
          indicatorActiveColor='#333'
          current={current}
          onChange={(e) => onSwiper(e)}
          circular
          indicatorDots
        >{
           data.spu_image_list && data.spu_image_list.map((i, idx) => {
              return (
                <SwiperItem className='item' key={idx}>
                  <View className='imgBox'>
                    <img className='bigImg' src={i} alt="" />
                  </View>
                </SwiperItem>
              )
            })
          }
        </Swiper>
        <View className='selectArr'>
          {
            data.spu_image_list && data.spu_image_list.map((i, idx) => {
              return (
                <View className='imgBox' key={idx} style={{ border: current == idx ? '1px solid' : '0px' }} onClick={() => onChangeSwiper(idx)}>
                  <img className='bigImg' src={i} alt="" />
                </View>
              )
            })
          }
        </View>
      </View>
    )
  }, [data, current])

  // 服务栏
  const ServiceBar = useMemo(() => {
    return (
      <View className='ServiceBar'>
        {
          ServiceData.map((i, idx) => {
            return (
              <View className='item' key={idx}>
                <img className='img' src={i.img}></img>
                <View className='name'>{i.name}</View>
              </View>
            )
          })
        }
      </View>
    )
  }, [ServiceData])

  // 尺寸下拉框
  const PickerDom = useMemo(() => {
    return (
      <Picker className="pickerDom" mode='selector' range={selector} value={rangeKey} onChange={onChangeSize}>
        <AtList
          hasBorder={false}
        >
          <AtListItem
            title={size}
            arrow='down'
            onClick={()=>{setKeySize(true)}}
          />
        </AtList>
      </Picker>
    )
  }, [selector])

  // 商品评论
  const ComtList = useMemo(() => {
    return comtData.map((i, idx) => {
      return (<View
        className="contList"
        style={{ paddingTop: idx == 0 && 0 }}
        key={idx}
      >
        <View className="cont-top">
          <View className="character">
            <img className='character-img' src={i.member.icon}></img>
            <View className='character-name'>{i.member.name}</View>
          </View>
          <View>{i.createdDate}</View>
        </View>
        <View className="cont-tail">
          <AtRate
            className='rateStar'
            value={rateNum}
            size={14}
          ></AtRate>
          <View className="section">{i.content}</View>
          <View className="img" >
            {
              i.imageUrls.map((item, index) => {
                return (
                  <img
                    key={index}
                    src={item}
                  ></img>
                )
              })
            }
          </View>
          {/* <View className="last">
            {i.productName && <View
            >{i.productName}</View>}
            
          </View> */}
        </View>
      </View >)
    })
  }, [comtData])

  // 商品参数
  const ShopParameter = useMemo(() =>{
    let value = data.attributes
    console.log('value', value)
    return (
      value && Object.keys( value ) && Object.keys( value ).map((i, idx) => {
        return (
          <View className='parameter'>
            {
              i === '包装机型' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '机芯' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '表壳形状' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '表带材质' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '表带针扣' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '表带颜色' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
            {
              i === '颜色' && (<View className='item'><img></img><View>{i}</View><View>{value[i][0]}</View></View>)
            }
          </View>
        )
      })
    )
  }, [])

  const DropDownList = useMemo(() => {
    return (
      openList.map((i, idx) => {
        return (
          <DropDown
            key={idx}
            open={i.open}
            onClick={() => dropDownClick(idx)}
            title={i.title}
            // icon={[search, '']}
          >
            {i.title == '商品描述' && <View className="cont1">
              {
                data.spu_description || (data.spu_detail_html && data.spu_detail_html.length) > 0 ? (
                  <View>
                    {
                      data.spu_description
                    }
                    {
                      data.spu_detail_html.length > 0 && data.spu_detail_html.map((item)=>{
                        return <img src={item}></img>
                      })
                    }
                  </View>
                ) : <View>{i.empty}</View>
              }
            </View>}
            {i.title == '商品评论' && <View className="cont2">
              {
                ComtList.length > 0 ? ComtList : <View>{i.empty}</View>
              }
            </View>}
            {i.title == '商品参数' && <View className="cont3">
              {
                ShopParameter
              }
            </View>}
            {i.title == '商品推荐' && <View className="cont4">
              <ShopList
                List={data.product_list}
                noWarp={true}
              ></ShopList>
            </View>}
          </DropDown >
        )
      })
    )
  }, [openList, dropDownClick, comtData, data])

  useEffect(() => {
    getShopDetailsFn()
  }, [])
  // const BuyCar = useMemo(()=>{
  //   return (
  //     <View className='shopBtn'>
  //       <View className='shopCar btn' onClick={onShopCar}>加入购物车</View>
  //       <View className='buy btn' onClick={onBuy}>立即购买</View>
  //     </View>
  //   )
  // }, [])
  return (
    <View className='shopDetail'>
      <Header
        leftUrl={[{ url: classify, type: 'Search'},{ url: search, type: ''}]}
        logo={logo}
        rightUrl={[{ url: user, type: ''},{ url: buyCar, type: ''}]}
      ></Header>
      {
        SwiperImg
      }
        <View style='text-align: center;     font-size: calc(28vmin * 100 / 750);
    font-weight: bolder;line-height: calc(50vmin * 100 / 750);    padding: 0 calc(80vmin * 100 / 750);'>
        <View className='goodsName'>{data.spu_name}</View>
        <View className='series'>系列: <View className='underLine'>{data.series_name}</View></View>
        <View className='rate'>
          <AtRate
            className='rateStar'
            value={rateNum}
            size={14}
          // onChange={this.handleChange.bind(this)}
          />
          <View className='underLine'>累计评论{'5'}</View>
        </View>
        <View className='price'>￥{data.max_sell_price}</View>
        <View className='number'>编号：{'GZ356'}</View>
        {/* {
          selector && (
            <View className='selector'>
              <View className='size'>
                <View className='title'>手围尺寸(CM)</View>
                <View className='guide underLine'>尺寸指南</View>
              </View>
              {PickerDom}
            </View>
          )
        } */}
        <View className='location'><img src={location} alt="" /><View className='underLine'>{'寻找精品店'}</View></View>
        {
          ServiceBar
        }
        </View>
      {
        DropDownList
      }
      <BtnGroup
        className="btnGroup"
        styleBtn={{ padding: 'calc(48vmin * 100 / 750) calc(40vmin * 100 / 750)' }}
        btn1={shopCar[0]}
        btn2={goBuy[0]}
        onClick1={onShopCar}
        onClick2={onBuy}
      ></BtnGroup>
      <Modal
        show={showSize}
        onCancel={onShowModal}
      >
        <View  
          className='content2'
        >
          <View
            className='title'
          >请确认你选择的尺寸为 {size}</View>
          <View
          ><View className='hand'>手围尺寸(CM)</View>{
            PickerDom
          }</View>
          <BtnGroup
            styleBtn={{ padding: 'calc(40vmin * 100 / 750) calc(10vmin * 100 / 750)', position: 'static', paddingTop: 'calc(200vmin * 100 / 750)' }}
            btn1={shopCar[1]}
            btn2={goBuy[1]}
            onClick1={onShopCar}
            onClick2={onBuy}
          ></BtnGroup>
        </View>
      </Modal>
      {/* <BackTop
      ></BackTop> */}
    </View >
  )
}
