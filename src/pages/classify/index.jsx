import { Component, useState, useMemo, useEffect } from 'react'
import Taro from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { useRequest } from 'taro-hooks';
import Header from '@/components/Header/index.jsx'

import './index.scss'

import classify from '@/assets/images/classify.png'
import back from '@/assets/images/back.png'
import logo from '@/assets/images/logo.png'
import close from '@/assets/images/close.png'

export default function Index() {
  const [data, setDate] = useState({})
  const { run: getProductFilter } = useRequest(
    {
      url: `/api/product/filter`,
      method: 'get',
    },
    {
      manual: true
    }
  )
  const getProductFilterFn = async () => {
    let res = await getProductFilter()
    if (res.data.data) {
      setDate(res.data.data)
    }
  }

  const goShopList = ((id, type) => {
    Taro.navigateTo({
      url: `pages/gategory/index?id=${id}&type=${type}`
    })
  })

  const SeriesDom = useMemo(() => {
    return (
      <View className='seriesDom'>
        {
          data.series_list && data.series_list.map((item, idx) => {
            return (
              <View
                key={item.id}
                className="item"
                onClick={() => goShopList(item.id)}
              >
                <img
                  className='img'
                  src={item.image}
                ></img>
                <View className='name'>{item.name}</View>
              </View>
            )
          })
        }
      </View>
    )
  }, [data.series_list])

  const classifyDom =useMemo(() => {
    return (
      data.category_list && data.category_list.map((item, idx) => {
        return (
          <View className='classifyDom'>
            <View
              key={item.id}
              className="title"
              onClick={() => goShopList(item.id)}
            >
              {item.name}
            </View>
            <View className='list'>
              {
                item.child_categories.length > 0 && item.child_categories.map((i, index)=>{
                  return (
                    <View
                      key={i.id}
                      className='child'
                      onClick={() => goShopList(i.id)}
                    >{i.name}</View>
                  )
                })                  
              }</View>
          </View>
        )
      })
    )
  }, [data.category_list])

  useEffect(() => {
    getProductFilterFn()
  }, [])


  return (
    <View className='Classify'>
      <Header
        leftUrl={[{ url: back, type: 'Back'}]}
        logo={logo}
        rightUrl={[{ url: close, type: ''}]}
      ></Header>
      <View
        className='content'
      >
        <View className='allSeries'>
          <View className='top'><View className='title'>浏览所有系列</View><View className='icon'>&gt;</View></View>
          {
            SeriesDom
          }
        </View>
        {
          classifyDom
        }
      </View>
    </View>
  )
}