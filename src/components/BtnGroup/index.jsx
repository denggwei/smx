import { Component, useState, useMemo } from 'react'
import { useRequest } from 'taro-hooks';
import { View, Text } from '@tarojs/components'

import './index.scss'

const BtnGroup = ({ styleBtn, btn1, btn2, onClick1, onClick2 }) => {

  return (
    <View className='btnGroup' style={styleBtn}>
      <View className='shopCar btn' onClick={onClick1} style={btn1.style}>{btn1.name}</View>
      <View className='buy btn' onClick={onClick2} style={btn2.style}>{btn2.name}</View>
    </View>
  )
}

export default BtnGroup;