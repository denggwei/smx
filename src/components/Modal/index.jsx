import { Component, useState, useMemo } from 'react'
import { useRequest } from 'taro-hooks';
import { View, Text, CoverView } from '@tarojs/components'
import { AtAccordion } from 'taro-ui'

import './index.scss'

const Modal = ({ show = false, onCancel, children }) => {


  return show && (
    <CoverView className='Modal'>
      <View className='content'>
        <View className='cancel' onClick={() => onCancel(false)}><View className='btn'>×</View></View>
        {
          children
        }
      </View>
    </CoverView>
  )
}

export default Modal;