import { Component, useState, useMemo } from 'react'
import Taro from '@tarojs/taro'
// import { useRequest } from '@tarojs/taro';
import { View, Text } from '@tarojs/components'

import './index.scss'

const Header = ({ leftUrl, logo, rightUrl }) => {

  const onSearch = () =>{
    Taro.navigateTo({
      url: 'pages/classify/index'
    })
  }
  const onBack = () =>{
    Taro.navigateBack()
  }

  const ComposeEvent = (type) => {
    switch(type){
      case 'Search':
        onSearch()
        break;
      case 'Back':
        onBack()
        break;
    }
  }

  // const logoView = useMemo(()=>{
  //   return (

  //   )
  // })

  const leftView = useMemo(()=>{
    return (
      <View className='leftView icon'>
        {
          leftUrl.map((item, idx)=>{
            return (
              <img
              key={idx}
                src={item.url}
                onClick={() => ComposeEvent(item.type)}
              ></img>)
          })
        }
      </View>
    )
  }, [leftUrl])

  const rightView = useMemo(()=>{
      return (
        <View className='rightView icon'>
          {
            rightUrl.map((item, idx)=>{
              return <img
              key={idx}
                src={item.url}
                onClick={() => ComposeEvent(item.type)}
              ></img>
            })
          }
        </View>
      )
  }, [rightUrl])


  return (
    <View className='header'>
      <View className='free'></View>
      <View className='tap'>
      {
        leftView
      }
      {logo && <img
        className='logo'
        src={logo}
      ></img>
      } 
      {
        rightView
      }
      </View>
    </View>
  )
}

export default Header;