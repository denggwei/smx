import { Component, useState, useMemo } from 'react'
import Taro from '@tarojs/taro'
// import { useRequest } from '@tarojs/taro';
import { View, Text,  Swiper, SwiperItem,ScrollView } from '@tarojs/components'

import './index.scss'

import wish from '@/assets/images/bag备份 6.png'

const ShopList = ({ List, noWarp }) => {
  const ListShop = useMemo(() => {
    return List.map((i, idx) => {
      return (
        <View
          key={idx}
          className='shopBox'
          onClick={() => Taro.navigateTo({
            url: 'pages/shopDetail/index?spuId=' + i.spu_id
          })}
        >
          <View className='shop_head'>
            <img className='new'></img>
            <img className='wish' src={wish}></img>
          </View>
          {/* <img
            src={i.spu_main_image
            }
            className='shop_img'
          ></img> */}
          <Swiper
            className='swiper'
            indicatorColor='#999'
            indicatorActiveColor='#333'
            current={0}
            // onChange={(e) => onSwiper(e)}
            circular
            indicatorDots
          >{
              i.spu_image_list.map((item, index) => {
                return (
                  <SwiperItem className='item' key={index}>
                    <View className='imgBox'>
                      <img className='bigImg' src={item} alt="" />
                    </View>
                  </SwiperItem>
                )
              })
            }
          </Swiper>
          <View className='shop_name'>{i.spu_name}</View>
          <View className='shop_style'>{i.series_name}</View>
          <View className='shop_price'>{i.max_sell_price}</View>
        </View>
      )
    })
  })

  return (
    <ScrollView className='shopList' style={noWarp ? { flexWrap: 'nowrap', overflowX: 'auto', padding: 'calc(31vmin * 100 / 750) calc(25vmin * 100 / 750)', } : ''}>
      {
        ListShop
      }
    </ScrollView>
  )
}

export default ShopList;