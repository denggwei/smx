import { Component, useState, useMemo } from 'react'
import { useRequest } from 'taro-hooks';
import { View, Text } from '@tarojs/components'

import { AtIcon } from 'taro-ui'
import './index.scss'

const DropDown = ({ open, onClick, title, icon, children }) => {

  

  return (
    <View className='dropDown'>
      <View className='title' onClick={()=>onClick()}>
        <View className='name'>{title}</View>
        {/* <img className='icon' src={open ? icon[1] : icon[0]}></img> */}
        {
          open ? <AtIcon value='subtract' size='30' color='#000'></AtIcon> : <AtIcon value='add' size='30' color='#000'></AtIcon>
        }
      </View>
      {open && <View
        className='content'
        >{
          children
        }</View>  
      }
    </View>
  )
}

export default DropDown;