import { Component, useState, useMemo, useEffect } from 'react'
import { useRequest } from '@tarojs/taro';
import { View, Text } from '@tarojs/components'


import { throttle } from '@/utils/index.js';
import './index.scss'

import backTop from '@/assets/images/椭圆形(1).png'

const BackTop = ({onBack }) => {
  // 定义 visibleBackTopBtn 变量控制 返回顶部 按钮的显隐
  const [visibleBackTopBtn, setVisibleBackTopBtn] = useState(true)


  // 点击按钮事件处理函数
  const backToTopHandle = () => {
    // 把页面滚动到页面顶部
    document.body.scrollTo({
      left: 0,
      top: 0,
      behavior: 'smooth'
    })
  }

  // const goBackTop = () => {
  //   setTimeout(() => {
  //     window.scrollTo(0, 0)
  //   })
  //   // document.body.scrollTo({
  //   //   left: 0,
  //   //   top: 0,
  //   //   behavior: 'smooth'
  //   // })
  // }

  return (
    <>
      {
        visibleBackTopBtn && 
        <View className='backTop' onClick={onBack}>
          <img src={backTop} alt="" />
        </View>
      }
    </>
  )
}

export default BackTop;