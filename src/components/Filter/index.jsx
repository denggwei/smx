import { Component, useState, useMemo, memo, useEffect } from 'react'
import { View, Text, CoverView } from '@tarojs/components'
import { AtAccordion } from 'taro-ui'

import './index.scss'

// import TestData from './a.json'

const Filter = memo(({ show, filterData, onChange }) => {
  console.log('加载')
  // const openObj = TestData.map((i, idx)=>{
  //   // const a = 
  //   const c = i.category.attributes.map((item, index)=>{
  //     return {
  //       title: item.attributeName,
  //     }
  //   })
  //   const d = i.category.specifications.map((item, index)=>{
  //     return {
  //       title: item.variationAttributeName,
  //     }`
  //   })
  // })

  const [accordionBol, setAccordionBol] = useState([{
    title: '品牌',
    open: true,
    attribute: 'brand_list'
  },{
    title: '商品系列',
    open: true,
    attribute: 'series_list'
  }])

  const initFilter = ((value, attribute)=>{
    filterData
    // let arr =  Object.keys(value).map((item)=>{
    //   return {title: item, open: true, attribute: attribute}
    // })
    // setAccordionBol([...accordionBol, ...arr])
  })

  const onAccordion = ((title)=>{
    console.log('title', title);
    let _accordionBol = accordionBol.map((item)=>{
      if(title === item.title){
        item.open = !item.open
      }
      return item
    })
    setAccordionBol([..._accordionBol])
  })

  const FilterDom = ((value, item) => {
    return (
      <View className='FilterList'>
        {
          value && value.map((i, idx) => {
            return (
              <View className='item' key={idx}>
                <View
                    className='FilterDom'
                    onClick={() => onChange(i, item)}
                >{i.name || i}</View>
              </View>
            )
          })
        }
      </View>
    )
  })

  const FitlerList = useMemo(() => {
    return accordionBol.map((item, idx)=>{
      return (
        (<AtAccordion
          key={idx}
          title={item.title}
          hasBorder={false}
          open={item.open}
          onClick={()=>onAccordion(item.title)}
        >{
          item.title === '品牌' || item.title === '商品系列' ? FilterDom(filterData[item.attribute], item) : FilterDom(filterData[item.attribute][item.title], item)
        }</AtAccordion>)
      )
    })
  }, [[...accordionBol]])

  // useEffect(()=>{    
  //   filterData.specification_list && initFilter(filterData.specification_list, 'specification_list')
    
  //   setTimeout(()=>{ 
  //     console.log('accordionBol', accordionBol);
  //     filterData.attribute_list && initFilter(filterData.attribute_list, 'attribute_list') 
  //   }, 1000)
  // }, [filterData])

  // useEffect(()=>{    
  //   filterData.attribute_list && initFilter(filterData.attribute_list, 'attribute_list')
  // }, [filterData])

  return show && (
    <CoverView className='Filter'>
      <View className='content'>
        {/* {
          BrandFitler()
        }
        {
          SeriesFitler()
        }
        {
          FitlerList(filterData.attribute_list)
        }
        {
          FitlerList(filterData.specification_list)
        } */}
        {
          FitlerList
        }
      </View>
    </CoverView>
  )
})

export default Filter;