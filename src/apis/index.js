import Log from "./log.js";
import Global from './global'
import Auth from './auth'
import Cache from './cache'
import Dialog from './dialog'
import Util from './util'
import ApiEnum from './api_enum'

//加载器文本
let _loadingTextDefault = '加载中..'
let api = {};

// 获取设备信息
let getDevice = () => {
	if (api._device) {
		return api._device
	}

	let deviceInfo = Taro.getSystemInfoSync()
	// '设备名称|设备版本|设备系统版本|设备当前使用语言|设备屏幕分辨率|http请求版本|设备唯一识别码|default'
	// 'MI 5|V1.0.0|6.0.1|zh|720*1280|1.0|320981221265414|default'
	let device = [
		deviceInfo.model,
		deviceInfo.version,
		deviceInfo.system,
		deviceInfo.language,
		deviceInfo.screenWidth + '*' + deviceInfo.screenHeight,
		'default',
		'default',
		'default'
	].join('|');

	api._device = device
	return device
}

// 获取请求时间，并格式化
let getRequestTime = () => {
	let time = new Date().getTime();
	let requestTime = Util.dateFormat(time, "yyyyMMddhhmmss");
	return requestTime;
}

// 获取随机数
let getRands = () => {
	let num = Math.random() * (999999 - 100000) + 100000;
	let rands = parseInt(num, 10);
	return rands
}

api.request = (code, data, method = 'GET', dataType = 'json', header = null) => {
	//设置请求头
	header = header || {}
<<<<<<< HEAD
	// const REQdevice = process.env.NODE_ENV == 'development' ? true : false
	// if(!REQdevice){
	// 	header['device'] = 'H5'
	// }
	header['Content-Type'] = 'application/json;charset=UTF-8'
=======
	header['Content-Type'] = 'application/json;charset=UTF-8'

>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	let head = {
		version: 'v1',
		device: getDevice(),
		reqtime: getRequestTime(),
		seqno: getRands(),
<<<<<<< HEAD
		sessionId: Auth.getSessionId(),
		device:'browser'
=======
		sessionId: Auth.getSessionId()
		// sessionId: '{\"loginId\":\"20302\",\"name\":\"测试用户_奥尔\",\"dtype\":\"BusinessLogin\",\"userId\":\"20302\"}'
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	}
	if ([ApiEnum.loginByCode, ApiEnum.loginByWxapp].includes(code)) {
		delete head.sessionId
	}

	// 1	reqtime	请求时间	N	X（14）	yyyymmddhhmmss
	// 2	seqno	请求随机数	N	X（6）	随机数
	// 3	device 	设备信息	N	X（100）	设备信息
	// 4	sessionId	会话	Y	X（20）	当需要识别请求者身份时必填
	// 5	version	请求版本号	Y	X	默认v1

	//设置请求参数
	data = data || {}
<<<<<<< HEAD
	//操作店铺
	if (!Array.isArray(data)) {
		data.storeId = "10852";
=======

	//操作店铺
	if (!Array.isArray(data)) {
		data.storeId = data.storeId || Global.server.app.storeId;
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	}

	//是否显示加载器
	let isShowLoading = data._isShowLoading || false
	let isPull = data._isPull || false
	let loadingText = data._loadingText || _loadingTextDefault
	let isReject = data._isReject || false

	//显示加载器
	if (isShowLoading) {
		isShowLoading = true
		Dialog.showLoading(loadingText)
	}

<<<<<<< HEAD
=======

>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	//删除字段
	delete data._isShowLoading
	delete data._isPull
	delete data._isReject
	delete data._loadingText

	let realData = {}
	realData.head = head
	realData.body = data

	return new Promise((resolve, reject) => {
<<<<<<< HEAD
		let reqUrl = Global.server.apiUrl + "/" + Global.server.miniappCode + "/" + code
		let reqData = method == 'POST' ? realData : data
		// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
		// uni.showLoading({
=======
		let reqsUrl = Global.server.webUrl + "/" + code + "/" + data.num
		let reqUrl = Global.server.apiUrl + "/" + Global.server.miniappCode + "/" + code
		let reqData = method == 'POST' ? realData : data
		reqUrl = data.isGateWay ? reqsUrl : reqUrl
		// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
		// Taro.showLoading({
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
		// 	title: '加载中',
		// 	mask: true
		// })
		//请求http
<<<<<<< HEAD
		uni.request({
=======
		Taro.request({
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
			header: header,
			url: reqUrl,
			data: reqData,
			dataType: dataType,
			responseType: 'text',
			method: method,
			success: (res) => {
				let response = res.data;
				// Log.i("===========resRequest>>>" + reqUrl + ">>>>" + JSON.stringify(response))
<<<<<<< HEAD
				if (response.head.bzflag == '200' && response.head.sysflag == '200') {
					if(response.body&&response.body.status_code=='401'){
						Auth.logout()
						Dialog.toast('登录信息已过期，请重新登录',()=>{
							uni.redirectTo({
								url:'/pages/mine'
							})
						})
						return
					}
					resolve(response.body)
=======
				if ((response && response.head && response.head.bzflag == '200') && (response && response.head.sysflag && response.head.sysflag == '200')) {
					resolve(response.body)
				} else if (response.code == 0) {
					resolve(response.rooms)
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
				} else {
					if (isReject) {
						reject(response.head)
						return
					}
					Dialog.alert({
						content: response.head.errmsg || '服务繁忙，请稍后再试...'
					})
				}
			},
			fail: (res) => {
				Log.e(res)
			},
			complete: () => {
				if (isPull) {
<<<<<<< HEAD
					uni.stopPullDownRefresh()
=======
					Taro.stopPullDownRefresh()
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
				}
				if (isShowLoading) {
					Dialog.hideLoading()
				}
<<<<<<< HEAD
				// uni.hideLoading()
=======
				// Taro.hideLoading()
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
			}
		})
	})
}

<<<<<<< HEAD
api.reqSc = (code, data, method = 'GET', dataType = 'json', header = null) => {
	// let deviceInfo = uni.getSystemInfoSync()
	// if(deviceInfo.platform=='mac'||deviceInfo.platform=='windows'||deviceInfo.platform=='linux'){
		
	// }
	
	header = header || {}
	header['x-org-id'] = '1443507816921366529'
	header['Authorization'] = 'Bearer ' + Auth.getAccess_token
	// #ifdef MP-WEIXIN
	header['x-channel'] = 1
	header['x-storefront-id'] = 1468471502209941505
	// #endif
	// #ifdef H5
	header['x-channel'] = 2
	header['x-storefront-id'] = 1468491070399709185
	// #endif
	let head = {}
	data = data || {}
	//是否显示加载器
	let isShowLoading = data._isShowLoading || false
=======
api.requestForBuriedPoint = (data, method = 'GET', dataType = 'json', header = null) => {
	//设置请求头
	header = header || {}
	header['Content-Type'] = 'application/json;charset=UTF-8'
	let dInfo = Taro.getSystemInfoSync() //获取设备信息
	delete dInfo.safeArea;
	let deviceString = JSON.stringify(dInfo)
	//系统版本号
	let osVer = dInfo.system.replace('Android ', '').replace('iOS ', '')
	//从本地获取guid 同步操作
	let guid = Taro.getStorageSync('guid')
	if (guid) {
		data.guid = guid
	}
	let openId = Taro.getStorageSync('openId')
	data.openId = openId

	let chanName = Taro.getStorageSync('chanName')
	let chanId = Taro.getStorageSync('chanId')
	if (!chanName) {
		chanName = '游客';
	}
	if (!chanId) {
		chanId = '000000000'
	}
	let head = {
		version: 'v1',
		device: getDevice(),
		reqtime: getRequestTime(),
		seqno: getRands(),
		sessionId: Auth.getSessionId(),

		url: data.url ? data.url : null, //当前数据链接地址
		prevUrl: data.prevUrl ? data.prevUrl : null, //上一页链接地址
		traceType: data.traceType, //埋点编号
		extraInfo: data.extraInfo ? JSON.stringify(data.extraInfo) : null, // 每一种类型具体的所需要参数
		openId: data.openId,
		appVer: dInfo.SDKVersion, //小程序基础库
		itfCode: data.itfCode, // 接口代码 
		device: deviceString,
		guid: data.guid, //统一标识 获取openId时会返回
		chanId: chanId, //上一个小程序的自定义编号   30030001
		chanName: chanName, //上一个小程序的名称     易采药
		storeId: Global.server.app.storeId, //店铺id
	}
	// console.log('埋点头部 header = ' + JSON.stringify(head)); 

	//设置请求参数
	data = data || {}

	//是否显示加载器
	let isShowLoading = false
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	let isPull = data._isPull || false
	let loadingText = data._loadingText || _loadingTextDefault
	let isReject = data._isReject || false

<<<<<<< HEAD
	//显示加载器
	if (isShowLoading) {
		isShowLoading = true
		Dialog.showLoading(loadingText)
	}

	let realData = {}
	realData.head = head
	realData.body = data

	return new Promise((resolve, reject) => {
		let reqUrl = Global.server.scApi + code
		let reqData = method == 'POST' ? realData : data
		uni.request({
			header: header,
			url: reqUrl,
			data: reqData,
=======
	//删除字段
	delete data._isShowLoading
	delete data._isPull
	delete data._isReject
	delete data._loadingText

	let realData = {}
	realData = head
	// let reqUrl = 'http://192.168.199.104:7080' + '/monitor/frontend/trace/save' //测试服务器
	let reqUrl = 'https://app.91syzl.com/track' + '/monitor/frontend/trace/save' //正式服务器
	let reqData = method == 'POST' ? realData : data
	// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
	//请求http
	Taro.request({
		header: header,
		url: reqUrl,
		data: reqData,
		dataType: dataType,
		responseType: 'text',
		method: method,
		success: (res) => {
			let response = res.data;
			// Log.i("===========resParam>>>" + reqUrl + ">>>>" + JSON.stringify(response))
		},
		fail: (res) => {
			Log.e(res)
		},
		complete: () => {
			if (isPull) {
				Taro.stopPullDownRefresh()
			}
		}
	})
}

api.requestForGuidchan = (data, method = 'GET', dataType = 'json', header = null) => {
	//设置请求头
	header = header || {}
	header['Content-Type'] = 'application/json;charset=UTF-8'

	let head = {
		code: data.code
	}
	console.log('requestForGuidchan header = ' + JSON.stringify(head));

	//设置请求参数
	data = data || {}

	//是否显示加载器
	let isShowLoading = false
	let isPull = data._isPull || false
	let loadingText = data._loadingText || _loadingTextDefault
	let isReject = data._isReject || false

	//删除字段
	delete data._isShowLoading
	delete data._isPull
	delete data._isReject
	delete data._loadingText

	let realData = {}
	realData = head
	return new Promise((resolve, reject) => {
		// let reqUrl = 'http://192.168.199.104:7080' + '/monitor/frontend/trace/save' //测试服务器
		let reqUrl = 'https://app.91syzl.com/track' + '/monitor/frontend/trace/guidchan' //正式服务器

		reqUrl = reqUrl + '?code=' + data.code
		let reqData = method == 'POST' ? realData : data
		// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
		//请求http
		Taro.request({
			header: header,
			url: reqUrl,
			data: data.code ? data.code : null,
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
			dataType: dataType,
			responseType: 'text',
			method: method,
			success: (res) => {
<<<<<<< HEAD
				console.log(res,"SC返回数据===========================================")
				let response = res.data;
				// Log.i("===========resRequest>>>" + reqUrl + ">>>>" + JSON.stringify(response))
				if (response.head.bzflag == '200' && response.head.sysflag == '200') {
					resolve(response.body)
				} else {
					if (isReject) {
						reject(response.head)
						return
					}
					Dialog.alert({
						content: response.head.errmsg || '服务繁忙，请稍后再试...'
					})
=======
				let response = res.data;
				// Log.i("===========resReuelt>>>" + reqUrl + ">>>>" + JSON.stringify(response))
				if (response.code === 200) {
					resolve(response.data)
				} else {
					resolve(res)
					// reject(res.msg);
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
				}
			},
			fail: (res) => {
				Log.e(res)
			},
			complete: () => {
				if (isPull) {
<<<<<<< HEAD
					uni.stopPullDownRefresh()
				}
				if (isShowLoading) {
					Dialog.hideLoading()
				}
				// uni.hideLoading()
=======
					Taro.stopPullDownRefresh()
				}
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
			}
		})
	})
}

<<<<<<< HEAD
// api.requestForBuriedPoint = (data, method = 'GET', dataType = 'json', header = null) => {
// 	//设置请求头
// 	header = header || {}
// 	header['Content-Type'] = 'application/json;charset=UTF-8'
// 	let dInfo = uni.getSystemInfoSync() //获取设备信息
// 	delete dInfo.safeArea;
// 	let deviceString = JSON.stringify(dInfo)
// 	//系统版本号
// 	let osVer = dInfo.system.replace('Android ', '').replace('iOS ', '')
// 	//从本地获取guid 同步操作
// 	let guid = uni.getStorageSync('guid')
// 	if (guid) {
// 		data.guid = guid
// 	}
// 	let openId = uni.getStorageSync('openId')
// 	data.openId = openId

// 	let chanName = uni.getStorageSync('chanName')
// 	let chanId = uni.getStorageSync('chanId')
// 	if (!chanName) {
// 		chanName = '游客';
// 	}
// 	if (!chanId) {
// 		chanId = '000000000'
// 	}
// 	let head = {
// 		version: 'v1',
// 		device: getDevice(),
// 		reqtime: getRequestTime(),
// 		seqno: getRands(),
// 		sessionId: Auth.getSessionId(),

// 		url: data.url ? data.url : null, //当前数据链接地址
// 		prevUrl: data.prevUrl ? data.prevUrl : null, //上一页链接地址
// 		traceType: data.traceType, //埋点编号
// 		extraInfo: data.extraInfo ? JSON.stringify(data.extraInfo) : null, // 每一种类型具体的所需要参数
// 		openId: data.openId,
// 		appVer: dInfo.SDKVersion, //小程序基础库
// 		itfCode: data.itfCode, // 接口代码 
// 		device: deviceString,
// 		guid: data.guid, //统一标识 获取openId时会返回
// 		chanId: chanId, //上一个小程序的自定义编号   30030001
// 		chanName: chanName, //上一个小程序的名称     易采药
// 		storeId: Global.server.app.storeId, //店铺id
// 	}
// 	// console.log('埋点头部 header = ' + JSON.stringify(head)); 

// 	//设置请求参数
// 	data = data || {}

// 	//是否显示加载器
// 	let isShowLoading = false
// 	let isPull = data._isPull || false
// 	let loadingText = data._loadingText || _loadingTextDefault
// 	let isReject = data._isReject || false

// 	//删除字段
// 	delete data._isShowLoading
// 	delete data._isPull
// 	delete data._isReject
// 	delete data._loadingText

// 	let realData = {}
// 	realData = head
// 	// let reqUrl = 'http://192.168.199.104:7080' + '/monitor/frontend/trace/save' //测试服务器
// 	let reqUrl = 'https://app.91syzl.com/track' + '/monitor/frontend/trace/save' //正式服务器
// 	let reqData = method == 'POST' ? realData : data
// 	// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
// 	//请求http
// 	uni.request({
// 		header: header,
// 		url: reqUrl,
// 		data: reqData,
// 		dataType: dataType,
// 		responseType: 'text',
// 		method: method,
// 		success: (res) => {
// 			let response = res.data;
// 			// Log.i("===========resParam>>>" + reqUrl + ">>>>" + JSON.stringify(response))
// 		},
// 		fail: (res) => {
// 			Log.e(res)
// 		},
// 		complete: () => {
// 			if (isPull) {
// 				uni.stopPullDownRefresh()
// 			}
// 		}
// 	})
// }

// api.requestForGuidchan = (data, method = 'GET', dataType = 'json', header = null) => {
// 	//设置请求头
// 	header = header || {}
// 	header['Content-Type'] = 'application/json;charset=UTF-8'

// 	let head = {
// 		code: data.code
// 	}
// 	console.log('requestForGuidchan header = ' + JSON.stringify(head));

// 	//设置请求参数
// 	data = data || {}

// 	//是否显示加载器
// 	let isShowLoading = false
// 	let isPull = data._isPull || false
// 	let loadingText = data._loadingText || _loadingTextDefault
// 	let isReject = data._isReject || false

// 	//删除字段
// 	delete data._isShowLoading
// 	delete data._isPull
// 	delete data._isReject
// 	delete data._loadingText

// 	let realData = {}
// 	realData = head
// 	return new Promise((resolve, reject) => {
// 		// let reqUrl = 'http://192.168.199.104:7080' + '/monitor/frontend/trace/save' //测试服务器
// 		let reqUrl = 'https://app.91syzl.com/track' + '/monitor/frontend/trace/guidchan' //正式服务器

// 		reqUrl = reqUrl + '?code=' + data.code
// 		let reqData = method == 'POST' ? realData : data
// 		// Log.i("===========reqParam>>>" + reqUrl + ">>>>" + JSON.stringify(reqData))
// 		//请求http
// 		uni.request({
// 			header: header,
// 			url: reqUrl,
// 			data: data.code ? data.code : null,
// 			dataType: dataType,
// 			responseType: 'text',
// 			method: method,
// 			success: (res) => {
// 				let response = res.data;
// 				// Log.i("===========resReuelt>>>" + reqUrl + ">>>>" + JSON.stringify(response))
// 				if (response.code === 200) {
// 					resolve(response.data)
// 				} else {
// 					reject(res.msg);
// 				}
// 			},
// 			fail: (res) => {
// 				Log.e(res)
// 			},
// 			complete: () => {
// 				if (isPull) {
// 					uni.stopPullDownRefresh()
// 				}
// 			}
// 		})
// 	})
// }

=======
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
// 包裹一层，方便后续拓展
api.requestWithTicket = (code, data, method = 'GET', dataType = 'json') => {
	// console.log('发起Get请求 code = '  + code + ' data = ' + JSON.stringify(data))
	return new Promise((resolve, reject) => {
		api.request(code, data, method, dataType)
			.then(resolve)
			.catch(reject)
	})
}

// 包裹一层，方便后续拓展
api.requestWithBuriedPoint = (data, method = 'GET', dataType = 'json') => {
	// console.log('发起Get请求 code = '  + code + ' data = ' + JSON.stringify(data)) 
	api.requestForBuriedPoint(data, method, dataType)
}

// 包裹一层，方便后续拓展
api.requestWithGuidchan = (data, method = 'GET', dataType = 'json') => {
	// console.log('发起Get请求 code = '  + code + ' data = ' + JSON.stringify(data))
	return new Promise((resolve, reject) => {
<<<<<<< HEAD
		api.requestForGuidchan(data, method, dataType).then(resolve).catch(reject)
=======
		api.requestForGuidchan(data, method, dataType)
			.then(resolve)
			.catch(reject)
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
	})
}

/**  
 * 上传图片(多张)
 * @param {string} uri 文件路径 [Y] 
 * @param {string} type 'multipart/form-data' [Y]
 * @param {string} name 文件key [Y]
 * 
 * @return {Array<Object>} body
 *    @return {string} id
 *    @return {string} name
 *    @return {string} path
 */
api.uploadImg = (data) => {

	data = data || {}

	let isShowLoading = data._isShowLoading || false
	let isReject = data._isReject || false
	if (isShowLoading) {
		Dialog.showLoading('上传中...')
	}
	return new Promise((resolve, reject) => {
		let header = {}
		header['Content-Type'] = 'multipart/form-data;'
<<<<<<< HEAD
		// const REQdevice = process.env.NODE_ENV == 'development' ? true : false
		// if(!REQdevice){
		// 	header['device'] = 'H5'
		// }
		let uploadUrl = Global.server.apiUrl + "/" + Global.server.miniappCode + "/" + ApiEnum.uploadImg
		console.log('上传链接 = ' + data.uri);
		uni.uploadFile({
			url: uploadUrl,
			filePath: data.uri,
			// header: header,
			name: data.name,
			formData: {
				// uri: data.uri,
				// type: 'multipart/form-data',
=======
		let uploadUrl = Global.server.apiUrl + "/" + Global.server.miniappCode + "/" + ApiEnum.uploadImg
		console.log('上传链接 = ' + data.uri);
		Taro.uploadFile({
			url: uploadUrl,
			filePath: data.uri,
			header: header,
			name: data.name,
			formData: {
				uri: data.uri,
				type: 'multipart/form-data',
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
				name: data.name
			},
			success: (res) => {
				console.log('图片上传成功');
				let ret = JSON.parse(res.data)
				if (res.statusCode == 200) {
					resolve(ret.body)
				} else {
					if (isReject) {
						reject(ret)
					}
				}
			},
			fail: (err) => {
				console.log('图片上传失败 = ' + JSON.stringify(err));
				if (isReject) {
					resolve(err)
				}
			},
			complete: () => {
				if (isShowLoading) {
					Dialog.hideLoading()
				}
			}
		})
	})
}

<<<<<<< HEAD
export default api
=======
export default api
>>>>>>> 6d7f40388cf226e5024001ff1683490a093d58c4
