import ApiBase from './index'
import ApiEnum from './config'

let api = Object.assign({}, ApiBase);

api.scApi = (data,url,method) => {
	let params = {
		"channel":2,
		"url":Global.server.scApi + url,
		"method": method||"POST",
		"token": Auth.getAccess_token(),
		"data": data
	}
	return api.requestWithTicket(ApiEnum.scApi, params, 'POST');
}

export default api