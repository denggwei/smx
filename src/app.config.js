export default {
  pages: [
    // 'pages/home/index',
    'pages/gategory/index',
    'pages/shopDetail/index',
    'pages/classify/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
}
